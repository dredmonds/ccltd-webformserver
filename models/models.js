/*****************************************************************
* Module Name: Mongoose Models
* Description: Mongoose Models of webForm server
* Author: 
* Date Modified: 2016.09.27
* Related files: n/a
*****************************************************************/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Models for mongoose to communicate to DB and as an object to play in Code.
exports.Users = new Schema({
  /*loginID:
  password:
  email:
  contactNo:
  contactFName:
  contactSName:
  companyName:
  EANno:
  VATno:
  addressLn1:
  addressLn2:
  addressLn3:
  addressLn4:
  postCode:
  countryCode:*/
});

exports.Messages = new Schema({
  msgID           : {type:Number, required: true, trim: true},
  tradingPartners : {type:String, required: true, trim: true},
  msgType         : {type:String, required: true, trim: true},
  docNumber       : {type:String, required: true, trim: true},
  docStatus       : {type:String, required: true, trim: true},
  createdDate     : {type:String, required: true, trim: true},
  actions         : {type:String, required: true, trim: true}
});

exports.Orders = new Schema({
  msgID         : {type:Number, required: true, trim: true},
  orderNumber   : {type:String, required: true, trim: true},
  orderDate     : {type:Date, required: true},
  customerName  : {type:String, required: true, trim: true},
  /*just only a few and we added more*/
  supplierName  : {type:String, required: true, trim: true},
  supplierEAN   : {type:String, required: true, trim: true},
  supplierCode  : {type:String, required: true, trim: true},
  /*just only a few and we added more*/
  addRemarks    : {type:String, required: true, trim: true},
  
  lineItems     : [{line             : {type:Number, trim: true},
                    supplierTUEAN    : {type:String, required: true, trim: true},
                    supplierItemCode : {type:String, required: true, trim: true},
                    buyerItemCode    : {type:String, required: true, trim: true},
                    CUinTU           : {type:String, required: true, trim: true},
                    quantity         : {type:String, required: true, trim: true},
                    itemPrice        : {type:String, required: true, trim: true},
                    description      : {type:String, required: true, trim: true}
				 }],
  /*just only a few and we added more*/
  orderStatus   : {type:String, required: true, trim: true},
  formStatus    : {type:String, required: true, trim: true},
  ediReference  : {type:String, required: true, trim: true},
  txnReference  : {type:String, required: true, trim: true},
  fileGenNumber : {type:String, required: true, trim: true}
});

exports.Invoices = new Schema({
  msgID         : {type:Number, required: true, trim: true},
  /*just only a few and we added more*/	
});

exports.DeliveryNotes = new Schema({
  msgID         : {type:Number, required: true, trim: true},
  /*just only a few and we added more*/
});

exports.TradingPartners = new Schema({
  /*companyName:
  EANno:
  VATno:
  delLocName:
  delLocEAN:
  delAddressLn1:
  delAddressLn2:
  delAddressLn3:
  delAddressLn4:
  postCode:
  countryCode:*/
});

exports.Products = new Schema({
  /*supplierTUEAN:
  supplierItemCode:
  buyerItemCode:
  CUinTU:
  itemPrice
  description*/
});